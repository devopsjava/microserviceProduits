INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (0, 'JEU DE 2 DISQUES DE FREIN', 'Notre Disque de frein avant de la marque BOLK, référence BOL-D011608 est compatible avec tous les véhicules de cette liste.', 'https://dam-media.mister-auto.com/bolk/disque-de-frein-avant/bol-d011608/240x240/999BOL-D011608-14-1.jpg', 15.90);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (1, 'FILTRE À CARBURANT', 'Notre Filtre à carburant de la marque BOLK, référence BOL-G061253 est compatible avec tous les véhicules de cette liste.', 'https://dam-media.mister-auto.com/bolk/filtre-a-carburant/bol-g061253/240x240/generic_9.png', 15.90);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (2, 'TRIANGLE DE SUSPENSION', 'Notre Triangle de suspension de la marque BOLK, référence BOL-B011498 est compatible avec tous les véhicules de cette liste. ', 'https://dam-media.mister-auto.com/bolk/triangle-de-suspension/bol-b011498/240x240/999BOL-B011498-14-1.jpg', 24.5);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (3, 'BATTERIE DE DÉMARRAGE 60AH / 540A', 'Ne passe pas au four', 'https://dam-media.mister-auto.com/bolk/batterie-de-voiture/bol-c021713e/240x240/999BOL-C021713E-15-1.jpg', 61.90);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (4, 'CATALYSEUR', 'Notre Catalyseur de la marque BOLK, référence BOL-G081197 est compatible avec tous les véhicules de cette liste.', 'https://dam-media.mister-auto.com/bolk/catalyseur/bol-g081197/240x240/999BOL-G081197-14-1.jpg', 227.90);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (5, 'COMPRESSEUR (CLIMATISATION)', 'Notre Compresseur de climatisation de la marque BOLK, référence BOL-C031459 est compatible avec tous les véhicules de cette liste.', 'https://dam-media.mister-auto.com/bolk/compresseur-de-climatisation/bol-c031459/240x240/999BOL-C031459-14-1.jpg', 180.90);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (6, 'RADIATEUR', 'Notre Radiateur de la marque BOLK, référence BOL-C011634 est compatible avec tous les véhicules de cette liste.', 'https://dam-media.mister-auto.com/bolk/radiateur/bol-c011634/240x240/999BOL-C011634-14-1.jpg', 53.90);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (7 , 'PHARE PRINCIPAL', 'Notre Phare principal de la marque BOLK, référence BOL-B061255 est compatible avec tous les véhicules de cette liste.', 'https://dam-media.mister-auto.com/bolk/phare-principal/bol-b061255/240x240/999BOL-B061255-14-1.jpg', 89.90);
